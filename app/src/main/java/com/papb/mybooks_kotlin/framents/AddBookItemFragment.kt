package com.papb.mybooks_kotlin.framents

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowId
import android.widget.Toast
import androidx.fragment.app.DialogFragment
import com.google.android.material.textfield.TextInputEditText
import com.papb.mybooks_kotlin.R
import com.papb.mybooks_kotlin.databinding.FragmentAddBookItemBinding
import com.papb.mybooks_kotlin.databinding.FragmentHomeBinding
import com.papb.mybooks_kotlin.utils.BooksData

class AddBookItemFragment : DialogFragment() {

    private lateinit var binding: FragmentAddBookItemBinding
    private lateinit var listener : DialogSaveBookBtnListeners
    private var bookData : BooksData? = null

    fun setListener(listener : DialogSaveBookBtnListeners){
        this.listener = listener
    }

    companion object{
        const val TAG = "AddBookPopupFragment"

        @JvmStatic
        fun newInstance(bookId: String, book:String) = AddBookItemFragment().apply {
            arguments = Bundle().apply {
                putString("bookId", bookId)
                putString("book", book)
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        binding = FragmentAddBookItemBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if(arguments != null){
            bookData = BooksData(
                arguments?.getString("bookId").toString(),
                arguments?.getString("book").toString()
            )

            binding.bookTitleEt.setText(bookData?.Buku)
        }
        registerEvent()
    }

    private fun registerEvent(){
        binding.saveBook.setOnClickListener(){
            val title = binding.bookTitleEt.text.toString()
            if (title.isNotEmpty()){

                if(bookData == null){
                    listener.onSaveBook(title, binding.bookTitleEt)
                }else{
                    bookData?.Buku = title
                    listener.onUpdateBook(bookData!!, binding.bookTitleEt)
                }

            }else{
                Toast.makeText(context,"Add your book title", Toast.LENGTH_SHORT).show()
            }
        }

        binding.todoClose.setOnClickListener(){
            dismiss()
        }
    }

    interface DialogSaveBookBtnListeners{
        fun onSaveBook(bookTit : String, bookTitleEt : TextInputEditText)
        fun onUpdateBook(booksData: BooksData, bookTitleEt : TextInputEditText)
    }

}