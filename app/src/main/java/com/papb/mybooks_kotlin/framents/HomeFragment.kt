package com.papb.mybooks_kotlin.framents

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Adapter
import android.widget.Toast
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.textfield.TextInputEditText
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.papb.mybooks_kotlin.R
import com.papb.mybooks_kotlin.databinding.FragmentHomeBinding
import com.papb.mybooks_kotlin.utils.BooksAdapter
import com.papb.mybooks_kotlin.utils.BooksData

class HomeFragment : Fragment(), AddBookItemFragment.DialogSaveBookBtnListeners,
    BooksAdapter.BooksAdapterClickInterface {

    private lateinit var auth : FirebaseAuth
    private lateinit var databaseReference: DatabaseReference
    private lateinit var navController: NavController
    private lateinit var binding: FragmentHomeBinding
    private var popUpFragment: AddBookItemFragment? = null
    private lateinit var adapter: BooksAdapter
    private lateinit var mList:MutableList<BooksData>

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentHomeBinding.inflate(inflater, container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        init(view)
        getDataFromFirebase()
        registerEvent()
    }

    private fun getDataFromFirebase() {
        databaseReference.addValueEventListener(object :ValueEventListener{
            override fun onDataChange(snapshot: DataSnapshot) {
               mList.clear()
                for (taskSnapshot in snapshot.children){
                    val books = taskSnapshot.key?.let{
                        BooksData(it, taskSnapshot.value.toString())
                    }

                    if (books != null){
                        mList.add(books)
                    }
                }
                adapter.notifyDataSetChanged()
            }

            override fun onCancelled(error: DatabaseError) {
                Toast.makeText(context, error.message, Toast.LENGTH_SHORT).show()
            }

        })
    }

    private fun registerEvent(){
        binding.addItemHome.setOnClickListener(){
            if (popUpFragment != null)
                childFragmentManager.beginTransaction().remove(popUpFragment!!).commit()
            popUpFragment = AddBookItemFragment()
            popUpFragment!!.setListener(this)
            popUpFragment!!.show(
                childFragmentManager,
                AddBookItemFragment.TAG
            )
        }
    }

    private fun init(view : View){
        navController = Navigation.findNavController(view)
        auth = FirebaseAuth.getInstance()
        databaseReference = FirebaseDatabase.getInstance()
            .reference.child("Buku").child(auth.currentUser?.uid.toString())


        binding.recycleView.setHasFixedSize(true)
        binding.recycleView.layoutManager = LinearLayoutManager(context)
        mList = mutableListOf()
        adapter = BooksAdapter(mList)
        adapter.setListener(this)

        binding.recycleView.adapter = adapter
    }

    override fun onSaveBook(bookTit : String, bookTitleEt : TextInputEditText) {

        databaseReference.push().setValue(bookTit).addOnCompleteListener(){
            if(it.isSuccessful){
                Toast.makeText(context, "Book Successfully Added!", Toast.LENGTH_SHORT).show()
                bookTitleEt.text = null
            }else{
                Toast.makeText(context, it.exception?.message, Toast.LENGTH_SHORT).show()
            }
            bookTitleEt.text = null
            popUpFragment!!.dismiss()

        }

    }

    override fun onUpdateBook(booksData: BooksData, bookTitleEt: TextInputEditText) {
        val map = HashMap<String, Any>()
        map[booksData.BukuId] = booksData.Buku
        databaseReference.updateChildren(map).addOnCompleteListener(){
            if (it.isSuccessful){
                Toast.makeText(context, "Updated Successfully", Toast.LENGTH_SHORT).show()
            }else{
                Toast.makeText(context, it.exception?.message, Toast.LENGTH_SHORT).show()
            }
            bookTitleEt.text = null
            popUpFragment!!.dismiss()
        }
    }

    override fun onDeleteBookBtnClicked(booksData: BooksData) {
        databaseReference.child(booksData.BukuId).removeValue().addOnCompleteListener(){
            if (it.isSuccessful){
                Toast.makeText(context, "Deleted Successfully", Toast.LENGTH_SHORT).show()
            }else{
                Toast.makeText(context, it.exception?.message, Toast.LENGTH_SHORT).show()
            }
        }
    }
    override fun onEditBookBtnClicked(booksData: BooksData) {
        if (popUpFragment != null)
            childFragmentManager.beginTransaction().remove(popUpFragment!!).commit()

        popUpFragment = AddBookItemFragment.newInstance(booksData.BukuId, booksData.Buku)
        popUpFragment!!.setListener(this)
        popUpFragment!!.show(childFragmentManager, AddBookItemFragment.TAG)
    }
}